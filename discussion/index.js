//array: used to store multiple related data

let hobbies = ["Play video games","Watch movies","Read books"];

console.log(hobbies);
console.log(typeof hobbies);

let nums = [1,2,3,4,5,6,7,8,9];
const vowels = ["a","e","i","o","u"];

let routine = ["drink coffee","eat breakfast","shower","work","play video games"];
let capital = ["Manila","Tokyo","Singapore","Jakarta"];

console.log(routine);
console.log(capital);

// .length property
routine.length = routine.length-1;
console.log(routine.length);
console.log(routine);

console.log(capital.length);
console.log("this is a string".length);

//accessing the elements of an array
//syntax: arrayName[index]

console.log(capital[0]);

let newCapital = capital[1];
console.log(newCapital);

capital[2] = "London";
console.log(capital);

//mini activity
let favoriteFoods = ["Tonkatsu","Adobo","Hamburger","Sinigang","Pizza"];
favoriteFoods[3] = "Fries";
favoriteFoods[4] = "Ramen";
console.log(favoriteFoods);

// looping over an array
for(let index = 0; index < favoriteFoods.length; index++){
    console.log(favoriteFoods[index]);
}

let numArr = [5,12,30,46,40];
for(let index = 0; index < numArr.length; index++){
    if(numArr[index] % 5 === 0){
        console.log(numArr[index] + " is divisible by 5");
    } else{
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// multidimensional array: arrays that contain other arrays
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard[1][5]);
console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);